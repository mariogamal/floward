package com.jimmy.posts.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jimmy.posts.R
import com.jimmy.posts.data.model.UserPosts
import com.squareup.picasso.Picasso

class UsersAdapter(
    private val userPostsList: List<UserPosts>,
    private val onClick: (userPost: UserPosts) -> Unit
) :
    RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val userImage: ImageView = view.findViewById(R.id.image)
        val userName: TextView = view.findViewById(R.id.name)
        val postCount: TextView = view.findViewById(R.id.count)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_user, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val user = userPostsList[position].user
        Picasso.get().load(user.thumbnailUrl).into(viewHolder.userImage)
        viewHolder.userName.text = user.name
        viewHolder.postCount.text = viewHolder.itemView.context
            .getString(R.string.post_count, userPostsList[position].posts.size)
        viewHolder.itemView.setOnClickListener {
            onClick(userPostsList[position])
        }
    }

    override fun getItemCount() = userPostsList.size

}
