package com.jimmy.posts.ui.main

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.jimmy.posts.R
import com.jimmy.posts.base.BaseActivity
import com.jimmy.posts.data.model.Post
import com.jimmy.posts.data.model.User
import com.jimmy.posts.data.model.UserPosts
import com.jimmy.posts.databinding.ActivityMainBinding
import com.jimmy.posts.ui.home.HomeFragment
import com.jimmy.posts.util.hide
import com.jimmy.posts.util.show
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {

    private val viewModel: MainViewModel by viewModel()
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        observe(viewModel.users) { users ->
            observe(viewModel.posts) { posts ->
                supportFragmentManager
                    .beginTransaction()
                    .add(R.id.container, HomeFragment(getUserPostsList(users, posts)))
                    .commit()
            }
        }
    }

    private fun getUserPostsList(users: List<User>, posts: List<Post>): List<UserPosts> {
        val userPostsMap = posts.groupBy { it.userId }
        val userPostsList = arrayListOf<UserPosts>()
        users.forEach { user ->
            userPostsList.add(UserPosts(user, userPostsMap[user.userId] ?: arrayListOf()))
        }
        return userPostsList
    }

    override fun showLoading() {
        binding.progressBar.show()
    }

    override fun hideLoading() {
        binding.progressBar.hide()
    }
}