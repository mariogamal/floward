package com.jimmy.posts.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.jimmy.posts.data.repository.MainRepository

class MainViewModel(private val repository: MainRepository): ViewModel() {
    val posts = liveData { emit(repository.getPosts()) }
    val users = liveData { emit(repository.getUsers()) }
}