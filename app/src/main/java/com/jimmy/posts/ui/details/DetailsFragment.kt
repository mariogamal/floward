package com.jimmy.posts.ui.details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jimmy.posts.data.model.UserPosts
import com.jimmy.posts.databinding.FragmentDetailsBinding
import com.squareup.picasso.Picasso

class DetailsFragment(private val userPosts: UserPosts) : Fragment() {

    lateinit var binding: FragmentDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Picasso.get().load(userPosts.user.url).into(binding.userImage)
        binding.postsRecycler.adapter = PostsAdapter(userPosts.posts)
    }
}