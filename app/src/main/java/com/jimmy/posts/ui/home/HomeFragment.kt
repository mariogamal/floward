package com.jimmy.posts.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jimmy.posts.ui.details.DetailsFragment
import com.jimmy.posts.R
import com.jimmy.posts.data.model.UserPosts
import com.jimmy.posts.databinding.FragmentHomeBinding

class HomeFragment(private val userPostsList: List<UserPosts>) : Fragment() {

    lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.usersRecycler.adapter = UsersAdapter(userPostsList){
            requireActivity().supportFragmentManager
                .beginTransaction()
                .add(R.id.container, DetailsFragment(it))
                .addToBackStack(null)
                .commit()
        }
    }

}