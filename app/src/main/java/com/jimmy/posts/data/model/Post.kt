package com.jimmy.posts.data.model

data class Post(val id: String, val userId: String, val title: String, val body: String)
