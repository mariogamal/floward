package com.jimmy.posts.data.remote

import com.jimmy.posts.data.model.Post
import com.jimmy.posts.data.model.User
import retrofit2.http.GET

interface PostsAPI {

    @GET("posts")
    suspend fun getPosts(): List<Post>

    @GET("users")
    suspend fun getUsers(): List<User>
}