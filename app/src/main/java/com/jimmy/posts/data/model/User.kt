package com.jimmy.posts.data.model

data class User(val userId: String, val name: String, val url: String, val thumbnailUrl: String)
