package com.jimmy.posts.data.model

data class UserPosts(val user: User, val posts: List<Post>)