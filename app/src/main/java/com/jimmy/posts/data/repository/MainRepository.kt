package com.jimmy.posts.data.repository

import com.jimmy.posts.base.BaseRepository
import com.jimmy.posts.data.remote.PostsAPI

class MainRepository(private val api: PostsAPI): BaseRepository() {

    suspend fun getPosts() = handleResponse { api.getPosts() }
    suspend fun getUsers() = handleResponse { api.getUsers() }

}