package com.jimmy.posts.di

import com.jimmy.posts.data.repository.MainRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory { MainRepository(get()) }
}