package com.jimmy.posts.base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import com.jimmy.posts.data.remote.Resource
import com.jimmy.posts.util.snack

abstract class BaseActivity: AppCompatActivity() {
    var rootView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootView = findViewById(android.R.id.content)
    }

    fun <T> observe(liveData: LiveData<Resource<T>>, result: (data: T) -> Unit) {
        liveData.observe(this) {
            when (it) {
                is Resource.Loading -> showLoading()
                is Resource.Success -> {
                    hideLoading()
                    it.data?.let { value -> result(value) }
                }
                is Resource.Error -> {
                    hideLoading()
                    it.message?.let { e -> showMessage(e) }
                }
            }
        }
    }

    open fun showMessage(message: String) {
        rootView?.snack(message)
    }

    abstract fun showLoading()

    abstract fun hideLoading()

}