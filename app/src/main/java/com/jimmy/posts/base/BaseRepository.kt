package com.jimmy.posts.base

import com.jimmy.posts.data.remote.Resource
import com.jimmy.posts.data.remote.ResponseHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

open class BaseRepository {

    suspend fun <T> handleResponse(apiMethod: suspend () -> T): Resource<T> =
        withContext(Dispatchers.IO) {
            try {
                ResponseHandler.handleSuccess(apiMethod())
            } catch (ex: Exception) {
                ResponseHandler.handleException(ex)
            }
        }

}