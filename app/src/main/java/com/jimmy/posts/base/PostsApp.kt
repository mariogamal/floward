package com.jimmy.posts.base

import android.app.Application
import com.jimmy.posts.di.apiModule
import com.jimmy.posts.di.repositoryModule
import com.jimmy.posts.di.retrofitModule
import com.jimmy.posts.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class PostsApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@PostsApp)
            modules(listOf(retrofitModule, apiModule, repositoryModule, viewModelModule))
        }
    }
}